import React, { Component } from "react";
import PropTypes from "prop-types";

class Tarea extends Component {

    cssCompletado(){
        return {
            fontSize : '20px',
            color : this.props.tarea.hecho ? 'red' : 'green',
            textDecoration : this.props.tarea.hecho ? 'line-through' : 'none',
        }
    }

    render() {
        const {tarea} = this.props; 
        return <div style={this.cssCompletado()}>
            <fieldset>
            <legend>{tarea.titulo}</legend>
            <label>Descripcion: </label> {tarea.descripcion}
            <label>Hecho: </label> {tarea.hecho}
            <label>ID: </label>{tarea.id}
            <input type="checkbox" checked={tarea.hecho} 
            onChange={this.props.fncCambiarEstado.bind(this, tarea.id)} />
            <button style={btnDelete} onClick={this.props.fncEliminarTarea.bind(this, tarea.id)} >X</button>
            </fieldset>
        </div>;
    }
}

Tarea.propTypes = {
    tarea: PropTypes.object.isRequired
}

const btnDelete = {
    fontSize : "18px",
    backgroundColor: '#ea2027',
    color : '#fff',
    border : 'none',
    padding : '10px 15px',
    borderRadius : '50%',
    cursor : 'pointer',
    float : 'right'
}
export default Tarea;