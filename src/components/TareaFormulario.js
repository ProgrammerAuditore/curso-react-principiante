import React, { Component } from "react";

class TareaFormulario extends Component {
    
    state ={
        titulo : '',
        descripcion : ''
    }

    fncOnSubmit = (e) => {
        e.preventDefault();
        this.props.agregarTarea(this.state.titulo, this.state.descripcion);
    }
    
    fncOnChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        });
    }

    render() {        
        return (
            <form onSubmit={this.fncOnSubmit}>
                <input 
                type="text" 
                name="titulo"
                placeholder="Escribe una tarea" 
                onChange={this.fncOnChange} 
                value={this.state.titulo} /> <br />
                <textarea 
                placeholder="Escribe una descripcion"
                name="descripcion" 
                onChange={this.fncOnChange} 
                value={this.state.descripcion} /> <br />
                <input type="submit" />
            </form>
        );
    }
}

export default TareaFormulario;