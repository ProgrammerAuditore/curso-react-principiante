import React, { Component } from "react";
import Tarea from "./Tarea";
import PropTypes from 'prop-types';

class Tareas extends Component {
    render(){
        return this.props.tareas.map( (tarea) => {
            return <Tarea  
            key={tarea.id}  
            tarea={ tarea }  
            fncEliminarTarea={this.props.fncEliminarTarea} 
            fncCambiarEstado={this.props.fncCambiarEstado}
            />
        });
    }
}

Tareas.propTypes = {
    tareas : PropTypes.array.isRequired
}

export default Tareas;