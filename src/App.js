import React, { Component } from 'react';
import './App.css';
import tareas from './sample/tasks.json';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

// Componentes
import Tareas from './components/Tareas';
import TareaFormulario from './components/TareaFormulario';
import Posts from './components/Posts';

class App extends Component {

  state = {
    tareas : tareas
  }

  fncAgregarTarea = (titulo, descripcion) => {
    const tarea = {
      id : this.state.tareas.length,
      titulo: titulo,
      descripcion: descripcion,
      hecho : false
    }

    this.setState({
      tareas: [...this.state.tareas, tarea]
    });
  }

  fncEliminarTarea = (id) => {
    const tareas = this.state.tareas.filter((tarea) => tarea.id !== id);
    console.log(tareas);
    this.setState({
      tareas : tareas
    });
  }
  
  fncCambiarEstado = (id) => {
    const tareas = this.state.tareas.map( (tarea) => {
      if(tarea.id === id){
        tarea.hecho = !tarea.hecho; 
      }
      return tarea;
    });

    this.setState({tareas : tareas});
  }

  render(){
    return (
      <div>
        <Router>

          <Link to="/">Home</Link> <br />
          <Link to="/posts">Posts</Link>

          <Route exact path="/" render={() => {  
            return <div>
              <TareaFormulario agregarTarea={this.fncAgregarTarea} />
              <Tareas 
              tareas={ this.state.tareas } 
              fncEliminarTarea={this.fncEliminarTarea}
              fncCambiarEstado={this.fncCambiarEstado} 
              />
              
            </div>
          }}>
          </Route>

          <Route exact path="/posts" component={Posts}/>
        </Router>

        
      </div>
    ); 
  }
}



export default App;
